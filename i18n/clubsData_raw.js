static get clubsData() {
    return {
      1: {
        id: "1",
        imgUrl: "#",
        type: "club",
        name: "Stripping club",
        days: "0",
        tier: "1",
        description: "Time to show off your sexy body.",
        perk1: "Reduce all task requirements by 8%",
        perk2: "Reduce all task requirements by 8%",
        job1:
          "Wear nothing but panties and a bra while performing your task(s).",
        job2:
          "In addition to Option 1 wear stockings and heels while performing your task(s)."
      },
      2: {
        id: "2",
        imgUrl: "#",
        type: "club",
        name: "Maid Club",
        days: "0",
        tier: "1",
        description: "We make house chores fun!",
        perk1: "Reduce all task requirements by 16%",
        perk2: "none",
        job1: "Wear a maid uniform while performing your task(s).",
        job2: "none"
      },
      3: {
        id: "3",
        imgUrl: "#",
        type: "club",
        name: "Audio club",
        days: "0",
        tier: "1",
        description:
          "We'll help you limit all distractions. (See Audio and Video section in Info page))",
        perk1: "Reduce all task requirements by 16%",
        perk2: "None",
        job1: "Listen to sex noise while performing your task(s).",
        job2: "None"
      },
      4: {
        id: "4",
        imgUrl: "#",
        type: "club",
        name: "Light-Bondage club",
        days: "0",
        tier: "1",
        description:
          "Make your classes more fun with some ropes and restraints.",
        perk1: "Reduce all task requirements by 8%",
        perk2: "Reduce all task requirements by 8%",
        job1:
          "Tie yourself in a Basic Rope Harness while performing your task(s).",
        job2: "Wear wrist and ankle cuffs while performing your task(s)."
      },
      5: {
        id: "5",
        imgUrl: "#",
        type: "club",
        name: "Heavy-Bondage club",
        days: "0",
        tier: "1",
        description: "Spice up your classes with various headgear.",
        perk1: "Reduce all task requirements by 8%",
        perk2: "Reduce all task requirements by 8%",
        job1: "Wear a gag while performing your task(s).",
        job2: "Wear a blindfold while performing your task(s)."
      },
      6: {
        id: "6",
        imgUrl: "#",
        type: "club",
        name: "SM Club",
        days: "0",
        tier: "1",
        description:
          "Discover the world of Sado Masochism. Too bad you're the only masochist here.",
        perk1: "Reduce all task requirements by 8%",
        perk2: "Reduce all task requirements by 8%",
        job1:
          "Attach clothespins to your nipples while performing your task(s).",
        job2: "Tie your cock and balls while performing your task(s)."
      },
      7: {
        id: "7",
        imgUrl: "#",
        type: "club",
        name: "Religious Studies Club",
        days: "0",
        tier: "1",
        description:
          "Thou shall preserve thine chastity and protect thine butthole.",
        perk1: "Reduce all task requirements by 8%",
        perk2: "Reduce all task requirements by 8%",
        job1: "Wear a chastity device while performing your task(s).",
        job2: "Wear a buttplug (size M/L) while performing your task(s)."
      },
      8: {
        id: "8",
        imgUrl: "#",
        type: "club",
        name: "Cosplay Club",
        days: "0",
        tier: "1",
        description:
          "Become an anime girl. (You can activate only one of the options)",
        perk1: "Reduce all task requirements by 16%",
        perk2: "Reduce all task requirements by 16%",
        job1:
          "Wear a japanese school uniform while performing your task(s).",
        job2:
          "Wear a cosplay uniform and wig while performing your task(s)."
      },
      9: {
        id: "9",
        imgUrl: "#",
        type: "club",
        name: "Experimental club",
        days: "0",
        tier: "2",
        description:
          "If you like trying out new stuff and experimenting with yourself you've come to the right place.",
        perk1: "Reduce all task requirements by 30%",
        perk2: "Skip 2 classes.",
        job1: "Roll 1 random punishment.",
        job2: "Roll 1 random punishment."
      },
      10: {
        id: "10",
        imgUrl: "#",
        type: "club",
        name: "Elite Students club",
        days: "0",
        tier: "2",
        description:
          "Are you a top student looking for some challenge? Become part of the school's elite and get access to special perks.",
        perk1: "Reduce all task requirements by 30% every day",
        perk2: "Skip 2 classes every day. (skips dont stack)",
        job1:
          "Increase required credits for graduating by 40 (increases once).",
        job2:
          "Increase required credits for graduating by 40 (increases once)."
      },
      11: {
        id: "11",
        imgUrl: "#",
        type: "club",
        name: "Science club",
        days: "0",
        tier: "2",
        description:
          "Time to learn some kink engineering with machines and electricity.",
        perk1: "Reduce all class requirements by 30%",
        perk2: "Skip 2 classes.",
        job1: "Spend 10 minutes with e-stim on your dick/pussy.",
        job2: "Spend 10 minutes on a fucking machine."
      },
      12: {
        id: "12",
        imgUrl: "#",
        type: "club",
        name: "Literature club",
        days: "0",
        tier: "2",
        description: "Read and write lewd fiction.",
        perk1: "Reduce all task requirements by 30%.",
        perk2: "Skip 2 classes today.",
        job1:
          "Write a short story about your task(s) and what a little slut you are. Share it on the internet or in chat.",
        job2: "Read 20 minutes of hentai today."
      },
      13: {
        id: "13",
        imgUrl: "#",
        type: "club",
        name: "Media Club",
        days: "0",
        tier: "2",
        description: "Time to become an e-thot.",
        perk1: "Reduce all task requirements by 30%",
        perk2: "Skip 2 classes",
        job1:
          "Take a picture while performing one of your daily tasks and share it to the public (on the internet or in chat.)",
        job2:
          "Record a video while performing one of your daily tasks and share it to the public (on the internet or in chat.)"
      },
      14: {
        id: "14",
        imgUrl: "#",
        type: "club",
        name: "Medical Club",
        days: "0",
        tier: "2",
        description: "You are so girly that even your blood tests say so!",
        perk1: "Reduce all task requirements by 30%",
        perk2: "Skip 2 classes",
        job1:
          "Drink your supplements (see supplements section in Info page)",
        job2: "Ensure that your hormones are within female norm."
      },
      15: {
        id: "15",
        imgUrl: "#",
        type: "club",
        name: "Lesbian Club",
        days: "0",
        tier: "2",
        description: "Cute girls doing cute things.",
        perk1: "Skip 2 classes.",
        perk2: "Reduce all task requirements by 30%.",
        job1:
          'Fuck another "girl" with a strapon. (If you have a penis it must be locked in chastity).',
        job2:
          'Let another "girl" fuck you with her dick/strapon. (If you have a penis it must be locked in chastity).'
      },
      16: {
        id: "16",
        imgUrl: "#",
        type: "club",
        name: "Boys Club",
        days: "0",
        tier: "2",
        description:
          "Bad boys doing bad things. Too bad you're a girl and you'll have to settle as being the club's whore.",
        perk1: "Skip 2 classes.",
        perk2: "Reduce all task requirements by 30%.",
        job1: "Give a blowjob to a real boy/man",
        job2: "Get fucked by a real boy/man"
      },
      17: {
        id: "17",
        imgUrl: "#",
        type: "club",
        name: "Community Service Club",
        days: "0",
        tier: "2",
        description: "Do what others tell you.",
        perk1: "Reduce all task requirements by 30%",
        perk2: "Skip 2 classes.",
        job1:
          "Request 1 lewd task in chat or online. You may re-request tasks up to three times.",
        job2:
          "Request 1 lewd task in chat or online. You may re-request tasks up to three times."
      },
      18: {
        id: "18",
        imgUrl: "#",
        type: "club",
        name: "Diaper Club",
        days: "0",
        tier: "1",
        description: "Wear diapers",
        perk1: "Reduce all task requirements by 16%",
        perk2: "none",
        job1: "Wear a diaper while performing your task(s).",
        job2: "none"
      },
      19: {
        id: "19",
        imgUrl: "#",
        type: "club",
        name: "Girls Club",
        days: "0",
        tier: "1",
        description: "Look attractive.",
        perk1: "Reduce all task requirements by 8%",
        perk2: "Reduce all task requirements by 8%",
        job1: "Wear slutty clothes/lingerie while performing your task(s).",
        job2: "Wear makeup while performing your task(s)."
      },
      20: {
        id: "20",
        imgUrl: "#",
        type: "club",
        name: "Hygiene Club",
        days: "0",
        tier: "1",
        description: "Feel smooth",
        perk1: "Reduce all task requirements by 8%",
        perk2: "Reduce all task requirements by 8%",
        job1: "Make sure your crotch area and armpits are shaved smooth.",
        job2: "Make sure your whole body is shaved smooth."
      },
      21: {
        id: "21",
        imgUrl: "#",
        type: "club",
        name: "Meditation Club",
        days: "0",
        tier: "2",
        description: "Spend more time with your thoughts.",
        perk1: "Reduce all task requirements by 30%",
        perk2: "Skip 2 classes.",
        job1:
          "Listen to static/white noise for 15 minutes while blindfolded, gagged and tied in a hogtie/frog-tie/lotus-tie/mummy-tie.",
        job2:
          "Listen to sex (porn) sounds for 15 minutes while blindfolded, gagged and tied in a hogtie/frog-tie/lotus-tie/mummy-tie."
      },
      22: {
        id: "22",
        imgUrl: "#",
        type: "club",
        name: "Kink Club",
        days: "0",
        tier: "1",
        description: "Play with some toys.",
        perk1: "Reduce all task requirements by 8%",
        perk2: "Reduce all task requirements by 8%",
        job1: "Insert a vibrator in your ass while performing you task(s).",
        job2:
          "Put a vibrator on your dick/clit while performing your task(s)."
      },
      23: {
        id: "23",
        imgUrl: "#",
        type: "club",
        name: "Exercise Club",
        days: "0",
        tier: "1",
        description: "Play with some toys.",
        perk1: "Reduce all task requirements by 8%",
        perk2: "Reduce all task requirements by 8%",
        job1:
          "Do exercises for 10 minutes. (Pushups, Squats, Jumping jacks, Burpees, Crunches).",
        job2: "Do 10 minutes of yoga/stretching."
      },
      24: {
        id: "24",
        imgUrl: "#",
        type: "club",
        name: "Fitness Club",
        days: "0",
        tier: "2",
        description: "Play with some toys.",
        perk1: "Reduce all task requirements by 30%",
        perk2: "Skip 2 classes.",
        job1: "Burn 400 calories from exercising today.",
        job2: "Exercise for 60minutes today."
      }
    };
  }