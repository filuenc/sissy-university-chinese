static get partnersData() {
  return {
    1: {
      id: "1",
      imgUrl: "#",
      type: "partner",
      name: "Zoe",
      name2: "anal oral",
      tier: "1",
      description:
        "Zoe is a second year Domination Studies major at the university and as such she needs to pick a play-partner for her penetration classes. Luckily she has noticed you and would love to have you as her sex toy for the rest of the year.",
      job1:
        "Increase the requirements for all Penetration tasks tasks by 25%",
      perk1:
        "Penetration tasks tasks will give you an additional 1 point.",
      job2:
        "Increase the requirements for all Penetration tasks tasks by 25%",
      perk2:
        "Penetration tasks tasks will give you an additional 1 point.",
      tags: "penetration"
    },
    2: {
      id: "2",
      imgUrl: "#",
      type: "partner",
      name: "Amie",
      name2: "feminization",
      tier: "1",
      description:
        "Amie is a third year Domination Studies student who's hates anything masculine. I guess this is how you'd describe a feminist? As such she likes to dress you up in her girly clothes and make you look as cute as possible.",
      job1: "Increase the requirements for feminization tasks by 25%",
      perk1: "Feminization tasks will give you an additional 1 point.",
      job2: "Increase the requirements for feminization tasks by 25%",
      perk2: "Feminization tasks will give you an additional 1 point.",
      tags: "feminization"
    },
    3: {
      id: "3",
      imgUrl: "#",
      type: "partner",
      name: "Charlotte",
      name2: "chastity",
      tier: "1",
      description:
        "Charlotte doesn't really study here but has volunteered to run the Religious Studies Club as she's a nun in the town's church. She's against any kinds of sexual pleasures as they're impure and since she doesn't want you to go to hell, she will permanently locked you in chastity so you won't give in to the temptation.",
      job1: "Increase the requirements for chastity tasks by 50%",
      perk1: "Chastity tasks will give you an additional 1 point.",
      job2: "Increase the requirements for chastity tasks by 50%",
      perk2: "Chastity tasks will give you an additional 1 point.",
      tags: "chastity"
    },
    4: {
      id: "4",
      imgUrl: "#",
      type: "partner",
      name: "Ina and Laura",
      name2: "orgasm",
      tier: "1",
      description:
        "Ina and Laura also known as The Orgasm Sisters, are two twins in their second year of Domination Studies. Although they seem fairly harmless you'll soon find out why they got that nickname.",
      job1:
        "Increase the requirements for edging/orgasm denial/orgasm torture tasks by 25%",
      perk1:
        "Edging/orgasm denial/orgasm torture tasks will give you an additional 1 point.",
      job2:
        "Increase the requirements for edging/orgasm denial/orgasm torture tasks by 25%",
      perk2:
        "Edging/orgasm denial/orgasm torture tasks will give you an additional 1 point.",
      tags: "orgasm"
    },
    5: {
      id: "5",
      imgUrl: "#",
      type: "partner",
      name: "Christina",
      name2: "bondage, slave, masochism",
      tier: "1",
      description:
        "Sometimes a dom, sometimes a sub Christina enjoys both sides of the spectrum. Having such rich experience she knows how to take care of her slaves gently when they submit but also how to punish them swiftly when they misbehave.",
      job1:
        "Increase the requirements for Bondage, Masochism and Slave tasks by 25%",
      perk1:
        "Bondage, Masochism and Slave tasks will give you an additional 1 point.",
      job2:
        "Increase the requirements for Bondage, Masochism and Slave tasks by 25%",
      perk2:
        "Bondage, Masochism and Slave tasks will give you an additional 1 point.",
      tags: "slave"
    },
    6: {
      id: "6",
      imgUrl: "#",
      type: "partner",
      name: "Mizuki",
      name2: "enema, watersports",
      tier: "1",
      description:
        "Mizuki is one the main nurses in school. While she takes good care of students she and her colleagues often get bored and hold students up for extended visits to perform various experiments on them. If you help her with her research she'll help you with some of your classes.",
      job1:
        "Increase the requirements for Enema and Watersports tasks by 25%.",
      perk1:
        "Enema and Watersports tasks will give you an additional 1 point.",
      job2:
        "Increase the requirements for Enema and Watersports tasks by 25%.",
      perk2:
        "Enema and Watersports tasks will give you an additional 1 point.",
      tags: "fluid"
    },
    8: {
      id: "8",
      imgUrl: "#",
      type: "partner",
      name: "Nina",
      name2: "pet-play, fetish",
      tier: "1",
      description:
        "Nina, has spend the last two years at the university specialising in pet training and fetish gear. With a kink for anything leather or latex she likes to keep her specimens tightly encased and always aroused. Most of the time you can find her at the back yard where she trains some of the aspiring pony girls. Nina expects full obedience from her pets but she's quite generous and caring. She'll even make you a collar with your name on it.",
      job1: "Increase the requirements for pet/animal tasks by 25%",
      perk1: "These tasks will give you an additional 1 point.",
      job2: "Increase the requirements for pet/animal tasks by 25%",
      perk2: "These tasks will give you an additional 1 point.",
      tags: "pet fetish"
    },
    11: {
      id: "11",
      imgUrl: "#",
      type: "partner",
      name: "Maya",
      name2: "all",
      tier: "1",
      description:
        "One of the top students in school and president of the student council - Maya. It's very surprising that she took a liking to you but now that it has happened you can't refuse. She expects nothing less than perfection and will push you to your limits.",
      job1: "Increase the requirements for all tasks by 25%",
      perk1: "All tasks will give you an additional 1 points.",
      job2: "Increase the requirements for all tasks by 25%",
      perk2: "All tasks will give you an additional 1 points.",
      tags: "all"
    },
    12: {
      id: "12",
      imgUrl: "#",
      type: "partner",
      name: "Yumeko",
      name2: "all",
      tier: "1",
      description:
        "Yumeko is the vice president of the student council and as such it's her responsibility to go through all the orgasm requests from students. Normally students are allowed to submit only one request per day but since you're buddies she'll let you try your luck a few more times but only if you manage to satisfy her sadistic needs.",
      job1: "Increase the requirements for all tasks by 10%",
      perk1:
        "Roll the orgasm roulette one more time. (Use after spinning the orgasm roulette and before starting any classes)",
      job2: "Increase the requirements for all tasks by 15%",
      perk2:
        "Roll the orgasm roulette one more time. (Use after spinning the orgasm roulette and before starting any classes)",
      tags: "justDifficulty"
    }
  };
}