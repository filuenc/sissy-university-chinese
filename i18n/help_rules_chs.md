# 帮助/规则

### 快速通知：

所有游戏数据都存储于你的本地浏览器中。除了排行榜之外，没有任何被储存或发送到线上。  
因为浏览器隐私模式下无法存储数据，所以开始之前请确定你使用的是普通浏览模式。另外，不要删除你的 cookies。

## 如何玩：

### 快速指南：

1. 选择一个主修专业。
1. 添加一些必修课程和额外的课程。
1. 添加一些社团与伙伴，这将让你的游戏变的更简单或更难。
1. 如果你想射精，你应该使用高潮轮盘。
1. 学习并通过课程，直到你拿到160学分。
1. 不要旷课，否则你将会遭到惩罚。
1. 毕业。

- 所有性玩具尺寸均为最小尺寸。性玩具标识尺寸是指玩具的最宽部分和总长度（可插入部分的长度通常小于总长度）。如果你愿意的话，你可以使用更大的。查看购物清单以获取更多信息。
- 你可以点击菜单栏（比如说上面的“如何玩”）以展开/折叠相应内容。
- 本页底部有一个紧凑的课程列表。

### 主修专业

根据你想专攻的内容选择主修专业。每个专业都有特定的必修课程（毕业的先决条件），每一门课程都有他们自己的前置课程。
选择一个主修专业之后，请进入“进度”页面查看必修课程。 在尝试毕业之前，你需要获得160学分。 如果你决定改变你的主修专业，你可以前往你所先择的专业页面放弃这个主修专业，如果你这样做，你将会被处以10次惩罚。

### 课程与计划

每一门课都有一个每周时间表，你需要追踪这个表格。你需要完成课程两项日常任务中的一项，或者参加这门课程的考试，才能被算做出席。如果你在一天结束之前没有出席这门课，一项随机的惩罚将会为你生成。如果你想参加考试，你必须至少出席一次，同时没有待完成的惩罚。你一旦完成了一门课程，只要你愿意，你可以立刻加入另一门课程。一旦你完成课程，你就不能再次参加这门课。你最多同时注册8门课程。不要同时参加太多课程，否则你将跟不上进度表。
**课程也可以在周末完成。周末课程是选修的，即使你翘掉它们，你也不会遭到惩罚。**

### 惩罚

- 如果你在一天结束之前没有出席这门课，一次随机的惩罚将会为你生成。
- 如果你没能完成一项任务，你必须按下“失败”按钮同时接受惩罚。
- 如果你暂停一项任务超过4小时，你将会遭到惩罚。
- 你可以选择你方便的时间完成惩罚。
- 你最多累积10次惩罚。
- 如果你有未完成的惩罚，你将不能毕业。所以请确保你没有累积太多的惩罚。
- 如果你没能完成日常任务，你将受到1次惩罚；未能通过考试，你将受到2次惩罚；未能完成毕业论文，你将受到5次惩罚。

### 社团

- 社团将会为你提供提供各种不同的奖励，诸如无惩罚的跳过课程、减少课程任务完成的要求。
- 社团是对课程的补充，因此你可以根据需求将它们组合起来。
- 一些社团任务与课程任务有所重叠（例如：宗教俱乐部与贞操课程）。在这种情况下，你可以免费享受社团奖励。
- 当你完成社团任务、享受一些社团奖励时，请确保你真正达到了社团的要求。比方说：如果你激活了一项要求你在任务期间佩戴口塞的社团奖励，此外还有一门时长为8小时的课程。由于你很可能无法佩戴8小时口塞，因此在开始这门课之前请关闭这项奖励。
- 你最多加入5个社团，同时激活10项社团奖励。
- 在游戏后期，你将解锁精英社团，精英社团可以提供更多的奖励，但同时也有着更严格的要求。
- 注意：你只能加入一个精英社团
- 普通社团奖励可以被任意激活与关闭，但精英社团的奖励每天只能被激活一次。
- 所有激活的奖励将在第二天被重置。
- 注意：从社团可以获得的最大减免量被限制在50%。比如说：你已经从普通社团获得了50%的减免量，你同时还从精英社团获得了25%的减免量，你仍将只得到50%的减免量而不是75%。

### 伙伴系统

伙伴非常类似社团，唯一的区别是他们让任务更难而不是更简单，但作为回报，你会得到额外的学分。你最多拥有三个伙伴。他们的奖励可以每天激活一次， 并会在第二天自动关闭。

### 任务与计时系统

这个游戏有一套为主修专业和课程服务的任务系统。在你“课程表”页面，你可以选择执行当天课程的任务。一旦选择了一个任务项，你将跳转至任务页面，在任务页面中，你可以选择开始计时器，或者这并不是一项有时间要求的任务，你可以点击“完成”按钮。计时器可以被暂停与恢复，另外计时器可以跨天运行。你将不会遭到惩罚，如果这项任务开始于昨天。如果你没能完成这项任务，你必须点击“失败”按钮同时接受惩罚。各个课程的计时器，你也可以在课程表页面相应课程条目处进行查看。

### 学分系统

毕业要求你至少有160个学分（除非另有说明）。据计算，您需要完成4个初级，5个中级，6个高级和2个硕士课程才能获得它们。 不同的活动会奖励您不同的学分。毕业要求你至少有160个学分（除非另有说明）。据计算，你需要完成4个初级，5个中级，6个高级和2个专家课程才能获得它们。不同的活动会奖励你不同的学分。

下面是奖励列表：

- 初级课程：出席：1学分；考试：3学分；旷课：-2学分；
- 中级课程：出席：2学分；考试：6学分；旷课：-4学分；
- 高级课程：出席：3学分；考试：9学分；旷课：-6学分；
- 专家课程：出席：4学分；考试：12学分；旷课：-8学分；
- 轻度责惩（惩罚）：重新选择：-1学分；失败：-2学分
- 重度责惩（惩罚）：重新选择：-2学分；失败：-4学分

### 毕业

一旦你完成了你所有的必修课程，同时通过参加额外课程或重复磨练必修课程，最终收集满160学分，你就可以尝试毕业。一些专业可能需要一个月才能完成最终任务。如果你没能达到任意一个最终任务要求，你必须要按下“失败”按钮，你将遭到10次惩罚。一些专业可能会有增加时限的额外惩罚。在你毕业之后，你的专业将会被标记为完成，所有的其它进度将会被重置。你可以在其它领域注册另一个专业，这将会对你将来的职业生涯十分的有用。

### 高潮轮盘系统

你被允许射精的唯一方法是，课程要求，或者通过课程表页面上方的高潮轮盘。你每天只有一次转动轮盘的机会。测试一下你的手气，你有：

- 30%的概率，禁止高潮
- 20%的概率，被挑逗然后禁止高潮
- 10%的概率，破灭高潮（不完全高潮）（ruined orgasm）
- 10%的概率，肛交高潮（anal orgasm）
- 10%的概率，正常高潮
- 10%的概率，获得一项随机的惩罚
- 10%的概率，询问线上的陌生人，你能否高潮

## 购物清单：

一些必备物品与可选物品的购物清单。链接为推荐物品，由你自己决定是否使用。小技巧：当你在 Aliexpress 购物时，请使用 “Aliexpress Standard Shipping”，虽然这将多花费1-2美元，但它提供物流跟踪及14-21天内交付。

- 假阴茎 2.5/14cm （直径/长度） - 这是S尺寸 [Link](https://www.aliexpress.com/item/2016-suction-cup-dildo-realistic-sex-toys-for-woman-penis-small-dildo-flesh-dildo-sex-products/32659778432.html)
- 假阴茎 3/15cm （直径/长度） - 这是M尺寸 [Link](https://www.aliexpress.com/item/Silicone-Realistic-Soft-Jelly-Dildo-G-Spot-Clitoris-Stimulation-Vibrator-Sex-Toys-for-Women-Sex-Product/32957880747.html)
- 假阴茎 4/21cm （直径/长度） - 这是L尺寸 [Link](https://www.aliexpress.com/item/Insertable-7-28-185mm-big-long-thick-dildo-fake-Penis-dong-realistic-artificial-cock-sex-products/32522231222.html)
- 假阴茎 4.5/23cm （直径/长度） - 这是XL尺寸 [Link](https://www.aliexpress.com/item/8-9-inch-23cm-long-d-4-5-cm-big-dildo-with-suction-cup-sex-penis/32726634784.html)
- 假阴茎 5/28cm （直径/长度） - 这是XXL尺寸 [Link](https://www.aliexpress.com/item/8-9-inch-23cm-long-d-4-5-cm-big-dildo-with-suction-cup-sex-penis/32726634784.html)
- S至L尺寸的假阴茎套装 [Link](https://www.aliexpress.com/item/strapless-dildo-vibrator-adult-sex-toys-for-women-big-dildos-machine-consolador-sex-products-huge-anal/32686810457.html)
- 假阴茎 3/34cm （直径/长度） - 这是超长尺寸（仅用于触手俱乐部） [Link](https://www.aliexpress.com/item/2016-Hot-Adult-Sex-Products-130-340mm-Flexible-Soft-Lesbian-Double-Dildo/32757096278.html)

<br>

- 肛塞 25mm （直径） - 这是S尺寸 [Link](https://www.aliexpress.com/item/1pcs-Set-Stainless-Steel-Metal-Anal-safe-plug-medical-Anal-Beads-Anus-tube-Crystal-Waterproof-Adult/32901354074.html)
- 肛塞 35mm （直径） - 这是M尺寸 [Link](https://www.aliexpress.com/item/1pcs-Set-Stainless-Steel-Metal-Anal-safe-plug-medical-Anal-Beads-Anus-tube-Crystal-Waterproof-Adult/32901354074.html)
- 肛塞 40mm （直径） - 这是L尺寸 [Link](https://www.aliexpress.com/item/1pcs-Set-Stainless-Steel-Metal-Anal-safe-plug-medical-Anal-Beads-Anus-tube-Crystal-Waterproof-Adult/32901354074.html)
- 肛塞 50mm （直径） - 这是XL尺寸 [Link](https://www.aliexpress.com/item/50mm-large-big-pyrex-glass-Anal-butt-plug-beads-ball-dildo-Sex-toys-Adult-products-for/32222809698.html)
- S至L尺寸的套装 [Link](https://www.aliexpress.com/item/Adult-Diary-Silicone-Anal-Plug-Jewelry-Dildo-Vibrator-Sex-Toys-for-Woman-Prostate-Massager-Bullet-Vibrador/32845831474.html)

<br>

- 贞操笼（用于贞节类课程）[Link](https://www.aliexpress.com/item/Prison-Bird-Amazing-Price-Small-Cage-The-100-Biosourced-resin-chastity-device-Cock-Cage-With-4/32767552450.html)
- 前列腺按摩器（用于强制榨精课程）[Link](https://www.aliexpress.com/item/Quality-Male-Prostate-Massager-G-spot-Anal-Butt-Plug-Anal-Masturbator-Prostate-Stimulator-Adult-Erotic-Toys/32787804324.html)
-  球型口塞（用于捆束类课程及社团）[Link](https://www.aliexpress.com/item/Adult-Games-Mouth-Gag-Silicone-Ball-Oral-Fixation-PU-Leather-Band-BDSM-Bondage-Restraints-4-Colors/32755021776.html)  - 环型口塞（可选，当你的嘴即需要被塞住，同时还需要被使用时）[Link](https://www.aliexpress.com/item/Mouth-Gag-Oral-Leather-Open-bondage-Bite-Pleasure-Couples-Flirt-Fetish-Erotic-Slave-Sex-Products-Toys/32801221928.html)
- 长筒袜或连裤袜，内裤，文胸，裙子或连衣裙，高跟鞋（用于女体化课程）。
- 假发，化妆套件（可选） -  眼罩，口塞，绳子，手铐和脚镣（用于捆束课程和社团）[Link](https://www.aliexpress.com/item/2018-new-7pcs-set-Sexy-Lingerie-PU-Leather-Sex-Bondage-Set-Toy-Bondage-Restraint-Handcuffs-Sex/32843455313.html)
- 女仆制服（用于女仆训练营） [套装1](https://www.aliexpress.com/item/Sexy-French-Maid-Costume-Sweet-Gothic-Lolita-Dress-Anime-Cosplay-Sissy-Maid-Uniform-Plus-Size-Halloween/32801590897.html) [套装2](https://www.aliexpress.com/item/New-Sexy-Lolita-French-Maid-Cosplay-Costume-Dress-Halloween/32777378271.html) [手套](https://www.aliexpress.com/item/Satin-Long-Gloves-Opera-Evening-Party-Prom-Costume-Fashion-Gloves/32662088095.html?spm=2114.13010708.0.0.32df4c4dwcs4XH) [长筒袜](https://www.aliexpress.com/item/1-Pair-Women-White-Stockings-Overknee-Winter-White-Stocking-Thigh-High-Knee-Hosiery-Cotton-Lace-Wave/32824743016.html) [连裤袜](https://www.aliexpress.com/item/Popular-in-Japan-Girl-Women-Sexy-velvet-Tights-fake-High-Stocking-Pantyhose-Mock-Bow-Suspender-High/32800607509.html)
- 尾巴肛塞（用于宠物养成课程）[Link](https://www.aliexpress.com/item/Sexy-Toys-Metal-Fake-Fur-Fox-Dog-Tail-Anal-Plug-Butt-Plug-BDSM-Flirt-Anus-Plug/32807557126.html)
- 动物耳朵（用于宠物养成课程）[LINK](https://www.aliexpress.com/item/1pair-2pcs-Cat-Ears-Hair-Clips-Cute-Animal-Hair-Hoop-Ornament-Trinket-bandeau-Make-Up-Tool/32832307703.html) [LINK2](https://www.aliexpress.com/item/Hot-Sale-Women-Girls-Fashion-Fox-Plush-cat-ears-Headbands-hair-Accessories/32817598830.html)
- 项圈（用于宠物养成课程）[LINK](https://www.aliexpress.com/item/New-4-color-PU-Leather-Plush-Neck-Sex-Collar-Fetish-Bondage-Adult-Games-Slave-Restraint-Flirting/32727168433.html)
- 乳胶紧身衣（用于恋物癖课程）[LINK](http://www.latexcatfish.com/xcart/El-Gimperator.html)
- AV棒（没有任何地方需要，只是为了好玩）[LINK](https://www.aliexpress.com/item/IKOKY-Big-Size-30-Speed-Vibrator-Powerful-Magic-Wand-Massager-Sex-Toys-for-Women-Clitoris-Stimulator/32839632762.html)
-  电击器械（用于科学俱乐部）：
    - 主机 [LINK](https://www.aliexpress.com/item/IKOKY-Therapy-Massager-Accessory-Electro-Stimulation-Electric-Dual-Output-Host-Pulse-Massage-Host-Electric-Shock/32889013470.html)
    - 电击乳夹子 [LINK](https://www.aliexpress.com/item/Electric-Shock-Bullet-Anal-Plug-Pulse-Vibrator-Electro-Stimulation-Nipple-Clamps-Clitoris-Penis-Last-Erect-Massager/32957469349.html)
    - 电击肛塞 [LINK](https://www.aliexpress.com/item/Electric-Shock-Bullet-Anal-Plug-Pulse-Vibrator-Electro-Stimulation-Nipple-Clamps-Clitoris-Penis-Last-Erect-Massager/32957469349.html)
    - 电击阴茎环 [LINK](https://www.aliexpress.com/item/E-Stim-Monopolar-Conductive-Loops-Cock-Rings-and-Ball-Electro-Penis-Ring-Rubber-Tube-TENS-Electrodes/32708220829.html)

### 补充项目：

可选的营养剂列表，增加前液渗出量与性欲。不会影响激素水平，无副作用。可在 [iHerb](https://iherb.com/) 上选购。

- 卵磷脂（Lecithin） - 每天1000-2000毫克，增加精液量。 购买前请选择从向日葵中提取的卵磷脂。大豆卵磷脂具有类雌激素作用。
- 吡啶甲酸锌（Zinc Picolinate） - 每隔一天50毫克。 增加勃起强度，性高潮强度，性欲，精液量。
- 太得恩（非洲臀果木提取物胶囊）（Pygeum） - 每天200毫克（100毫克，每天两次），增加精液量，并改善前列腺健康。
- L-精氨酸（L-Arginine） - 每天5000mg（5gr），提高勃起强度。但经常有副作用，我尽量会避免使用它。
- 水 - 喝很多水。每天最少2升（0.52加仑）。

## 其它

### 附加信息：

- 一些可以在线分享与请求任务的地方：
    - [The Sissy University Discord Chat](https://discord.gg/bYpvdfG)
    - [4chan](https://boards.4chan.org/b/catalog)
    - [Reddit - Chastity](https://www.reddit.com/r/Chastity/)
    - [Reddit - Sissies](https://www.reddit.com/r/Sissies/)
    - [Reddit - BDSM gone wild](https://www.reddit.com/r/BDSMGW/)
    - [One Submissive Act](https://onesubmissiveact.com/)
- 出于卫生或健康原因，你可随时移除肛塞和贞操笼。但不要滥用这个。
- 所有玩具尺寸均为最小尺寸。如果你愿意，可以使用更大的。肛塞的直径是在最宽的部分测量的。
- 当你通过肛门达到高潮时，你的肛交任务可以视为完成了，除非另有说明。
- 您可以同时进行来自不同课程与社团的多项任务（如果可能的话）。
- 灌肠量请遵照相关的医疗健康指南。
- 你可以使用任何你喜欢的配方，来作为假精液。
- 可爱的内衣特指胸罩和内裤
- 可爱的衣服通常是指女性长筒袜，紧身裤，裙子，连衣裙，女式上衣，束腰，紧身胸衣和高跟鞋。
- 恋物癖装备是由乳胶/皮革/pvc/乙烯基/塑料制成的任何衣服（包括手套/口罩/头套）。 束腰（任何材料）和6英寸（15厘米）以上的高跟鞋也算在其中。小的配件不算。
- “全包紧身衣”是指紧身衣+手套，袜子和头套。仅仅一个“紧身衣”可能没有他们。*译者注：对于这条提示，可以参考[这条维基](https://en.wikipedia.org/wiki/Catsuit)。*

### 教程：

[龟甲缚教程](http://www.symtoys.com/ideas_bondkar1.html)  
[驷马缚教程](https://www.likera.com/sb/tech/hogtie1.php)  
[高级驷马教程](https://www.likera.com/sb/tech/hogtie2.php)  
[变种驷马](https://sissy-university.com/img/frogtie1.jpg) [效果图](https://sissy-university.com/img/frogtie2.jpg)  
[木乃伊教程](https://www.likera.com/blog/wp/archives/13432)  
[上吊结打法](https://www.google.com/search?tbm=isch&q=cinch+noose)

简单的"bitchsuit" *译者注：bitchsuit 不知道该怎么翻译，想知道 bitchsuit 可以参考[这个页面](https://www.pornhub.com/view_video.php?viewkey=ph595dc511a468c)*

- 绑住你的每条腿（将脚踝绑到臀部），你可以使用绳索或腰带。然后用绳子或者手拷将两只手腕连接起来，然后把绳索（手拷）拉到脖子后面。你可以参考下面的图片。

![bitchsuit](https://sissy-university.com/img/bitchsuit.jpg)

## 音频与视频文件

### 催眠

*一些推荐的音频与视频。由你决定是否使用。也可以看这里：[8chan - hypno board](https://8ch.net/hypno/catalog.html)*

催眠音频 - Bambi Bimbodoll Conditioning (Entry Level) [INFO](https://bambisleep.blogspot.com/2017/02/bambi-bimbodoll-conditioning.html) [DOWNLOAD](https://drive.google.com/file/d/0B8vLYK1NM6HbWGVaOVZLQTEtVE0/edit)  
催眠音频 - Bambi Training Loops (Upper Entry Level) [INFO](https://bambisleep.blogspot.com/2017/05/bambi-training-loops.html) [DOWNLAOD](https://drive.google.com/file/d/0B8vLYK1NM6HbUmstZXZFZC05cGM/edit)  
催眠音频 - Bambi Fuckdoll Brainwash (Advanced) [INFO](https://bambisleep.blogspot.com/2017/06/bambi-fuckdoll-brainwash.html) [DOWNLAOD](https://drive.google.com/file/d/0B8vLYK1NM6HbUnMzWFVwMzVvMDQ/edit)  
催眠音频 - Mistress Catgirl files [LINK](https://hypno-fetish.com/music/2311/mistress-catgirl/popout/1)  
催眠音频/视频 - [Cat Girl Hypnosis](https://youtu.be/ZrcxbJaXCCs)  
催眠音频/视频 - [Dog Hypnosis](https://www.youtube.com/watch?v=ArIf5pklBYY)  
催眠视频 - [Training & Feminization 1](https://www.pornhub.com/view_video.php?viewkey=ph56258bf3be096)  
催眠视频 - [Ultimate Feminizer](https://www.pornhub.com/view_video.php?viewkey=ph579080b599f23)  
催眠视频 - [Sissified Masturbation](https://www.pornhub.com/view_video.php?viewkey=ph59ea059b3e2d4)  

### 传授/动员 （Instructions/Motivation）

暂无

### 性噪声

*（用于感官剥夺课程与音响俱乐部）*

- 15分钟混响 - [Google Drive 地址](https://drive.google.com/open?id=1P8yTSAEMDEDfLjZYJvm25UDdQl_o5LXw)

## 紧凑课程列表（过时的）

![course_list](https://sissy-university.com/img/course_list.svg)


