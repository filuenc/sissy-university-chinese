static get classesData() {
  return {
    101: {
      id: "101",
      imgUrl: "#",
      type: "class",
      name: "液体 101",
      name2: "灌肠 圣水",
      prerequisites: "",
      days: "1 5",
      description: "你将学习和不同液体玩耍。",
      tier: "beginner",
      opt1: "灌入250毫升灌肠液(任何液体)，保持4分钟。",
      opt2: "尿在你自己身上。",
      pass1: "灌入500毫升灌肠液(任何液体)，保持8分钟。",
      pass2: "尿在你的脸上。",
      tags: "fluid"
    },
    201: {
      id: "201",
      imgUrl: "#",
      type: "class",
      name: "液体play 201",
      name2: "灌肠 圣水",
      prerequisites: "101",
      days: "1 5",
      description:
        "吞下不同的液体使你舒适。",
      tier: "intermediate",
      opt1: "灌入500毫升灌肠液(任何液体)，保持5分钟。",
      opt2: "尿在你的脸上。",
      pass1: "灌入500毫升灌肠液(任何液体)，保持10分钟",
      pass2: "尿在你的嘴里。",
      tags: "fluid"
    },
    301: {
      id: "301",
      imgUrl: "#",
      type: "class",
      name: "液体进阶 301",
      name2: "灌肠 圣水",
      prerequisites: "201",
      days: "1 5",
      description: "液体是你的新宠物。",
      tier: "advanced",
      opt1:
        "灌入500毫升灌肠液(任何液体)，保持10分钟。",
      opt2: "尿在你的嘴里。",
      pass1: "灌入750毫升灌肠液(任何液体)，保持12分钟。",
      pass2: "喝至少50毫升（1.7盎司）的小便。",
      tags: "fluid"
    },
    401: {
      id: "401",
      imgUrl: "#",
      type: "class",
      name: "流体 401",
      name2: "灌肠 圣水",
      prerequisites: "301",
      days: "1 5",
      description: "您将学习如何容纳大量液体。",
      tier: "master",
      opt1:
        "灌入750毫升灌肠液(任何液体)，保持10分钟。",
      opt2: "喝至少50毫升（1.7盎司）的小便。",
      pass1: "灌入1000毫升灌肠液(任何液体)，保持15分钟。",
      pass2: "喝至少100毫升（3.3盎司）的小便。",
      tags: "fluid"
    },
    102: {
      id: "102",
      imgUrl: "#",
      type: "class",
      name: "菊穴 102",
      name2: "菊穴",
      prerequisites: "",
      days: "1 3 5",
      description: "你将发现新的愉悦区。",
      tier: "beginner",
      opt1: "用假阳具(S码)操自己的菊穴5分钟。",
      opt2: "佩戴肛塞(S码)30分钟。",
      pass1: "用假阳具(S码)操自己的菊穴10分钟。",
      pass2: "佩戴肛塞(S码)60分钟。",
      tags: "penetration"
    },
    302: {
      id: "302",
      imgUrl: "#",
      type: "class",
      name: "菊穴进阶 302",
      name2: "菊穴",
      prerequisites: "102",
      days: "1 3 5",
      description:
        "学习如何用菊穴接受这些 —— 更快、更深、更大。",
      tier: "advanced",
      opt1: "用假阳具(M码)操自己的菊穴15分钟。",
      opt2: "用假阳具(L码)操自己的菊穴10分钟。",
      pass1: "用假阳具(M码)操自己的菊穴30分钟。",
      pass2: "用假阳具(L码)操自己的菊穴20分钟。.",
      tags: "penetration"
    },
    402: {
      id: "402",
      imgUrl: "#",
      type: "class",
      name: "深入菊穴 402",
      name2: "菊穴",
      prerequisites: "302",
      days: "1 3 5",
      description: "你将学会只从菊穴获得愉悦。",
      tier: "master",
      opt1: "用假阳具(M码)操自己的菊穴30分钟。",
      opt2: "用假阳具(L码)操自己的菊穴20分钟。.",
      pass1: "用假阳具(L码)操自己的菊穴60分钟。",
      pass2: "用假阳具(XL码)操自己的菊穴40分钟。",
      tags: "penetration"
    },
    103: {
      id: "103",
      imgUrl: "#",
      type: "class",
      name: "禁欲 103",
      name2: "贞洁",
      prerequisites: "",
      days: "2 3 4",
      description:
        "你将会适应贞操笼的束缚和和禁止高潮的命令。",
      tier: "beginner",
      opt1: "今天偑戴120分钟贞操笼。",
      opt2: "偑戴30分钟贞操笼，同时观看色情影片。",
      pass1: "今天偑戴240分钟贞操笼。",
      pass2: "偑戴60分钟贞操笼，同时观看色情影片。",
      tags: "chastity"
    },
    203: {
      id: "203",
      imgUrl: "#",
      type: "class",
      name: "禁欲 203",
      name2: "贞洁",
      prerequisites: "103",
      days: "1 2 3 4",
      description:
        "你将被调教成喜欢被长期锁在贞操笼中。",
      tier: "intermediate",
      opt1: "今天偑戴360分钟贞操笼。",
      opt2:
        "今天偑戴240分钟贞操笼。在上锁期间，至少观看30分钟色情影片。",
      pass1: "偑戴480分钟贞操笼。",
      pass2: "今晚佩戴着贞操笼睡觉。",
      tags: "chastity"
    },
    303: {
      id: "303",
      imgUrl: "#",
      type: "class",
      name: "长期禁欲 303",
      name2: "贞洁",
      prerequisites: "203",
      days: "1 2 3 4",
      description: "你将会遗忘射精是什么感觉。",
      tier: "advanced",
      opt1: "偑戴1380分钟贞操笼。",
      opt2:
        "偑戴720分钟贞操笼。 在上锁期间，至少观看40分钟色情影片。",
      pass1:
        "偑戴1380分钟贞操笼。 在上锁期间，至少观看60分钟色情影片。",
      pass2:
        "将你自己锁在贞操笼中，直到你从菊穴达到高潮，才能解锁。",
      tags: "chastity"
    },
    403: {
      id: "403",
      imgUrl: "#",
      type: "class",
      name: "永久禁欲 403",
      name2: "贞洁",
      prerequisites: "303",
      days: "1 2 3 4 5",
      description: "阴蒂被锁着已经是你的本能了。",
      tier: "master",
      opt1: "偑戴1440分钟贞操笼。",
      opt2: "偑戴1440分钟贞操笼。",
      pass1: "偑戴10080分钟贞操笼",
      pass2:
        "将你自己锁在贞操笼中，直到你从菊穴达到两次高潮，才能解锁。",
      tags: "chastity"
    },
    104: {
      id: "104",
      imgUrl: "#",
      type: "class",
      name: "女性气质 104",
      name2: "女体化",
      prerequisites: "",
      days: "2 3 4",
      description:
        '你将开始发掘你女性的一面。（服装信息见“帮助/规则”中的“其他”部分）',
      tier: "beginner",
      opt1: "今天穿60分钟女式内裤。",
      opt2: "今天穿60分钟文胸。",
      pass1: "今天穿120分钟可爱的内衣。",
      pass2:
        "一边听30分钟催眠音频，一边穿着可爱的内衣。",
      tags: "feminization"
    },
    204: {
      id: "204",
      imgUrl: "#",
      type: "class",
      name: "变装 204",
      name2: "女体化",
      prerequisites: "104",
      days: "1 2 3 4",
      description:
        '你将开始学习像个女孩一样穿衣和思考。（服装信息见“帮助/规则”中的“其他”部分）',
      tier: "intermediate",
      opt1:
        "一边听30分钟的催眠音频，一边穿着可爱的内衣和一件其它的可爱衣服。",
      opt2:
        "今天穿可爱的内衣和一件其它的可爱衣服120分钟。",
      pass1:
        "今天穿可爱的内衣和两件其它的可爱衣服120分钟。 听30分钟的催眠音频。",
      pass2: "刮干净你的胯部、腋窝。",
      tags: "feminization"
    },
    304: {
      id: "304",
      imgUrl: "#",
      type: "class",
      name: "娘化 304",
      name2: "女体化",
      prerequisites: "204",
      days: "1 2 3 4",
      description: "你会穿得像个女孩，脑子里想的也像个女孩。",
      tier: "advanced",
      opt1:
        "今天穿可爱的内衣和两件其它的可爱衣服120分钟。 听30分钟的催眠音频。",
      opt2:
        "今天穿可爱的内衣和三件其它的可爱衣服180分钟。",
      pass1:
        "今天穿可爱的内衣和三件其它的可爱衣服240分钟。 听45分钟的催眠音频。",
      pass2:
        "把你的腿毛刮干净。穿可爱的内衣和三件其它的可爱衣服，化妆，戴假发(长发)120分钟。",
      tags: "feminization"
    },
    404: {
      id: "404",
      imgUrl: "#",
      type: "class",
      name: "性别重置 404",
      name2: "女体化",
      prerequisites: "304",
      days: "1 2 3 4 5",
      description:
        "这门课结束后，没有人能分辨出你是不是一个男孩，即使是你自己。",
      tier: "master",
      opt1:
        "今天穿可爱的内衣和可爱衣服360分钟。 听40分钟的催眠音频",
      opt2:
        "今天穿上可爱的内衣、可爱的衣服、高跟鞋、化妆、戴上假发(长发)240分钟。",
      pass1:
        "今天穿可爱的内衣和可爱衣服1440分钟。 听60分钟的催眠音频",
      pass2:
        "刮掉全身的体毛。今天穿可爱的内衣，可爱的衣服，高跟鞋，化妆和假发(长发)720分钟。",
      tags: "feminization"
    },
    105: {
      id: "105",
      imgUrl: "#",
      type: "class",
      name: "口交 105",
      name2: "口穴",
      prerequisites: "",
      days: "1 3 5",
      description:
        "你将学习如何用你嘴巴取悦一个鸡巴。",
      tier: "beginner",
      opt1: "给假阳具(S码)口交5分钟。",
      opt2: "用假阳具(S码)深喉（吞进去10cm以上）5次。",
      pass1: "给假阳具(S码)口交10分钟。",
      pass2: "用假阳具(S码)深喉（吞进去10cm以上）10次。",
      tags: "penetration"
    },
    305: {
      id: "305",
      imgUrl: "#",
      type: "class",
      name: "吹萧 305",
      name2: "口穴",
      prerequisites: "105",
      days: "1 3 5",
      description: "你将学习如何深喉。",
      tier: "advanced",
      opt1: "给假阳具(M码)口交10分钟。",
      opt2:
        "用假阳具(M码)深喉（吞进去15cm以上）10次。Hold your last deepthroat for 3 seconds.",
      pass1: "给假阳具(M码)口交15分钟。",
      pass2:
        "用假阳具(M码)深喉（吞进去15cm以上）20次。Hold at least two deepthroats for at least 6 seconds.",
      tags: "penetration"
    },
    405: {
      id: "405",
      imgUrl: "#",
      type: "class",
      name: "深喉练习 405",
      name2: "口穴",
      prerequisites: "305",
      days: "1 3 5",
      description:
        "你的口穴将比你的菊穴更会取悦一只鸡巴。",
      tier: "master",
      opt1:
        "Deepthroat (19cm/~7.5inch) a dildo (size L) 20times in one sitting.",
      opt2:
        "Deepthroat (19cm/~7.5inch) a dildo (size L) for 5 seconds straight - 5times.",
      pass1:
        "Deepthroat (19cm/~7.5inch) a dildo (size L) 30times in one sitting. ",
      pass2:
        "Deepthroat (19cm/~7.5inch) a dildo (size L) for 8 seconds straight - 10times.",
      tags: "penetration"
    },
    106: {
      id: "106",
      imgUrl: "#",
      type: "class",
      name: "Cum Play 106",
      name2: "精液play",
      prerequisites: "",
      days: "1 3",
      description: "You will learn to play with your cum.",
      tier: "beginner",
      opt1: "Lick any precum that's left from your other activities.",
      opt2: "Give yourself a facial with cum (real or fake).",
      pass1: "Drink a cum load (real or fake).",
      pass2:
        "Use cum (real or fake) as lube for one of your oral or anal activities.",
      tags: "fluid"
    },
    306: {
      id: "306",
      imgUrl: "#",
      type: "class",
      name: "Cum Appreciation 306",
      name2: "精液play",
      prerequisites: "106",
      days: "1 3",
      description: "You will learn to enjoy cum.",
      tier: "advanced",
      opt1: "Give yourself a facial with cum (real or fake).",
      opt2:
        "Use cum (real or fake) as lube for one of your oral or anal activities.",
      pass1:
        "Use cum (real or fake) as lube for one of your daily activities. Drink a cum load (real or fake) after you're done.",
      pass2:
        "Put cum (real or fake) inside a condom and then put it in your mouth for 5 minutes without tying it.",
      tags: "fluid"
    },
    406: {
      id: "406",
      imgUrl: "#",
      type: "class",
      name: "Cum Addiction 406",
      name2: "精液play",
      prerequisites: "306",
      days: "1 3 5",
      description: "Cum will be your most delicious treat.",
      tier: "master",
      opt1: "Drink a cum load (real or fake).",
      opt2:
        "Use cum (real or fake) as lube for all of your daily activities.",
      pass1:
        "Drink a cum load (real or fake). Then use cum as lube for one of your daily tasks. Drink another load of cum when you're finished.",
      pass2:
        "Give yourself a cum facial for 20 minutes and drink another load of cum in the meantime.",
      tags: "fluid"
    },
    107: {
      id: "107",
      imgUrl: "#",
      type: "class",
      name: "Vaginal Insertions 107",
      name2: "女性专属",
      prerequisites: "",
      days: "1 3 5",
      description:
        "You will learn to play with your pussy. (This is a female only class)",
      tier: "beginner",
      opt1: "Fuck your pusy for 5minutes with a dildo. (size S)",
      opt2: "Insert a dildo (size S) in your pussy for 10minutes.",
      pass1: "Fuck your pussy for 10minutes with a dildo (size S)",
      pass2: "Insert a dildo (size M) in your pussy for 15minutes.",
      tags: "female"
    },
    207: {
      id: "207",
      imgUrl: "#",
      type: "class",
      name: "Pussy Stretching 207",
      name2: "女性专属",
      prerequisites: "107",
      days: "1 3 5",
      description:
        "You will stretch your dirty cunt. (This is a female only class)",
      tier: "intermediate",
      opt1: "Fuck your pussy for 10minutes with a dildo (size S)",
      opt2: "Insert a dildo (size M) in your pussy for 15minutes.",
      pass1: "Fuck your pussy for 10minutes with a dildo (size M)",
      pass2: "Insert a dildo (size L) in your pussy for 15minutes.",
      tags: "female"
    },
    307: {
      id: "307",
      imgUrl: "#",
      type: "class",
      name: "Female Training 307",
      name2: "女性专属",
      prerequisites: "207 215",
      days: "2 4",
      description:
        "You will put your pussy to work. (This is a female only class)",
      tier: "advanced",
      opt1: "Fuck your pussy for 15minutes with a dildo. (size M).",
      opt2: "Fuck your pussy for 10minutes with a dildo. (size L)",
      pass1: "Fuck your pussy for 30minutes with a dildo. (size M).",
      pass2: "Fuck your pussy for 20minutes with a dildo. (size L).",
      tags: "female"
    },
    407: {
      id: "407",
      imgUrl: "#",
      type: "class",
      name: "Feminism 407",
      name2: "女性专属",
      prerequisites: "307",
      days: "2 4",
      description:
        "You will experience sexual empowerment and liberation. (This is a female only class)",
      tier: "master",
      opt1: "用假阳具(M码)操自己的菊穴30分钟。",
      opt2: "用假阳具(L码)操自己的菊穴20分钟。.",
      pass1: "用假阳具(L码)操自己的菊穴60分钟。",
      pass2: "用假阳具(XL码)操自己的菊穴40分钟。",
      tags: "female"
    },
    118: {
      id: "118",
      imgUrl: "#",
      type: "class",
      name: "Exotic Materials 108",
      name2: "恋物癖",
      prerequisites: "",
      days: "2 3 4",
      description:
        'You will learn to work with exotic materials such as latex, leather, PVC and vinyl. (See "Others" in "Info" for clothing info)',
      tier: "beginner",
      opt1: "Wear a fetish piece of clothing for 120minutes today.",
      opt2: "Wear two fetish pieces of clothing for 60minutes.",
      pass1: "Wear a fetish piece of clothing for 180minutes today.",
      pass2: "Wear two fetish pieces of clothing for 120minutes today.",
      tags: "fetish"
    },
    218: {
      id: "218",
      imgUrl: "#",
      type: "class",
      name: "Fetish Materials 208",
      name2: "恋物癖",
      prerequisites: "118",
      days: "2 3 4",
      description:
        "You will dress in fetish materials such as latex, leather, PVC and vinyl.",
      tier: "intermediate",
      opt1: "Wear two fetish pieces of clothing for 120minutes today.",
      opt2: "Wear three fetish pieces of clothing for 60minutes today.",
      pass1:
        "Wear three fetish pieces of clothing  for 120minutes today.",
      pass2: "Wear four fetish pieces of clothing for 60minutes today.",
      tags: "fetish"
    },
    318: {
      id: "318",
      imgUrl: "#",
      type: "class",
      name: "Fetish Clothing 308",
      name2: "恋物癖",
      prerequisites: "218",
      days: "1 3 4",
      description:
        "You will start exploring the world of fetish clothing. (Corsets also count)",
      tier: "advanced",
      opt1: "Wear four fetish pieces of clothing for 120minutes today.",
      opt2: "Wear a catsuit (bodysuit) for 120minutes today.",
      pass1: "Wear four fetish pieces of clothing for 240minutes today.",
      pass2: "Wear a catsuit (bodysuit) for 180minutes today.",
      tags: "fetish"
    },
    418: {
      id: "418",
      imgUrl: "#",
      type: "class",
      name: "Fetishism 408",
      name2: "恋物癖",
      prerequisites: "318",
      days: "2 3 4",
      description: "You will become a true fetishist.",
      tier: "master",
      opt1:
        "Wear four fetish pieces of clothing or a catsuit for 180minutes today.",
      opt2:
        "Wear a catsuit (bodysuit) and a hood/mask for 120minutes today.",
      pass1:
        "Wear four fetish pieces of clothing or a catsuit for 240minutes today.",
      pass2:
        "Wear a catsuit (bodysuit) and a hood/mask for 180minutes today.",
      tags: "fetish"
    },
    209: {
      id: "209",
      imgUrl: "#",
      type: "class",
      name: "Orgasm Training 209",
      name2: "性高潮",
      prerequisites: "",
      days: "6 0",
      description:
        "You will learn to orgasm multiple times. (This is a weekend only class)",
      tier: "intermediate",
      opt1:
        "Cum once today but continue all stimulation for 15 seconds after orgasm.",
      opt2: "Cum 2 times today.",
      pass1:
        "Cum once today but continue all stimulation for 30 seconds after orgasm.",
      pass2: "Cum 3 times today.",
      tags: "orgasm"
    },
    309: {
      id: "309",
      imgUrl: "#",
      type: "class",
      name: "Forced Orgasms 309",
      name2: "性高潮",
      prerequisites: "209",
      days: "6 0",
      description:
        "You'll be forced to orgasm multiple times. (This is a weekend only class)",
      tier: "advanced",
      opt1:
        "Cum once today but continue all stimulation for 30 seconds after orgasm.",
      opt2: "Cum 2 times in 30minutes.",
      pass1:
        "Cum once today but continue all stimulation for 60 seconds after orgasm.",
      pass2: "Cum 3 times in 45minutes.",
      tags: "orgasm"
    },
    409: {
      id: "409",
      imgUrl: "#",
      type: "class",
      name: "Orgasm Torture 409",
      name2: "性高潮",
      prerequisites: "309",
      days: "6 0",
      description:
        "You'll be forced to cum against your will. (This is a weekend only class)",
      tier: "master",
      opt1:
        "Cum once today but continue all stimulation for 60 seconds after orgasm.",
      opt2: "Cum 3 times in 45minutes.",
      pass1:
        "Cum once today but continue all stimulation for 2 minutes after orgasm.",
      pass2: "Cum 4 times in 60minutes.",
      tags: "orgasm"
    },
    213: {
      id: "213",
      imgUrl: "#",
      type: "class",
      name: "Edging 110",
      name2: "高潮禁止",
      prerequisites: "",
      days: "1 3 4",
      description:
        "You will learn to hold back your orgasms. (If you are locked in chastity, unlock yourself to complete your task. Don't forget to re-lock yourself after)",
      tier: "beginner",
      opt1: "Masturbate for 10minutes.",
      opt2: "Edge 2times.",
      pass1: "Masturbate for 20minutes.",
      pass2: "Edge 4times.",
      tags: "orgasm"
    },
    313: {
      id: "313",
      imgUrl: "#",
      type: "class",
      name: "Tease and Denial 310",
      name2: "高潮禁止",
      prerequisites: "213",
      days: "1 2 3 4",
      description:
        "You will learn to stay on the edge and deny your orgasms. (If you are locked in chastity, unlock yourself to complete your class workload. Don't forget to re-lock yourself after)",
      tier: "advanced",
      opt1:
        "Masturbate for 20minutes and edge 2times. No cumming allowed today.",
      opt2: "Edge 3times. No cumming allowed today.",
      pass1:
        "Masturbate for 30minutes and edge 4times. No cumming allowed today.",
      pass2: "Edge 5times. No cumming allowed today.",
      tags: "orgasm"
    },
    413: {
      id: "413",
      imgUrl: "#",
      type: "class",
      name: "Orgasm Denial 410",
      name2: "高潮禁止",
      prerequisites: "313",
      days: "1 2 3 4",
      description:
        "Sexual frustration is your new natural state. (If you are locked in chastity, unlock yourself to complete your class workload. Don't forget to re-lock yourself after)",
      tier: "master",
      opt1:
        "Masturbate for 30minutes and edge 3times. No cumming allowed today.",
      opt2: "Edge 4times. No cumming allowed today.",
      pass1:
        "Masturbate for 45minutes and edge 6times. No cumming allowed today.",
      pass2: "Edge 8times. No cumming allowed today.",
      tags: "orgasm"
    },
    211: {
      id: "211",
      imgUrl: "#",
      type: "class",
      name: "Prostate Play 211",
      name2: "菊穴",
      prerequisites: "102",
      days: "1 4",
      description:
        "You will learn how to feel pleasure from your prostate.",
      tier: "intermediate",
      opt1:
        "Fuck yourself for 10minutes with a dildo (size M). Aim for your prostate.",
      opt2:
        "Fuck yourself for with a dildo (size M) until you start leaking.  Aim for your prostate.",
      pass1:
        "Fuck yourself 20minutes with a dildo (size M). Aim for your prostate.",
      pass2:
        "Fuck yourself for with a dildo (size M) until you start leaking. Then continue for another 10 minutes. Aim for your prostate.",
      tags: "anal"
    },
    311: {
      id: "311",
      imgUrl: "#",
      type: "class",
      name: "Prostate Milking 311",
      name2: "菊穴",
      prerequisites: "211",
      days: "1 4",
      description: "You will learn how to milk your prostate.",
      tier: "advanced",
      opt1:
        "Milk your prostate with a vibrator or a prostate massager for 30minutes.",
      opt2:
        "Milk your prostate with a vibrator or a prostate massager until you start leaking. Then continue for 10 minutes.",
      pass1:
        "Milk your prostate with a vibrator or a prostate massager for 60minutes.",
      pass2:
        "Milk your prostate with a vibrator or a prostate massager until you cum.",
      tags: "penetration"
    },
    212: {
      id: "212",
      imgUrl: "#",
      type: "class",
      name: "Buttplug Training 212",
      name2: "菊穴",
      prerequisites: "102",
      days: "1 3",
      description: "You will expand your anal horizons.",
      tier: "intermediate",
      opt1: "Wear a buttplug (size S) for 90minutes today.",
      opt2: "Wear a buttplug (size M) for 60minutes today.",
      pass1: "Wear a buttplug (size S) for 180minutes today.",
      pass2: "Wear a buttplug (size M) for 120minutes today.",
      tags: "penetration"
    },
    312: {
      id: "312",
      imgUrl: "#",
      type: "class",
      name: "Buttplug Stretching 312",
      name2: "菊穴",
      prerequisites: "212",
      days: "1 3",
      description:
        "You will expand your anal horizons with a larger set of plugs.",
      tier: "advanced",
      opt1: "Wear a buttplug (size M) for 180minutes.",
      opt2: "Wear a buttplug (size L) for 120minutes.",
      pass1: "Wear a buttplug (size L) for 240minutes.",
      pass2: "Wear a buttplug (size XL) for 180minutes.",
      tags: "penetration"
    },
    214: {
      id: "214",
      imgUrl: "#",
      type: "class",
      name: "Bondage 214",
      name2: "奴隶",
      prerequisites: "",
      days: "3 4",
      description: "You will learn the basics of self-bondage.",
      tier: "intermediate",
      opt1: "Cuff your legs together for 20minutes.",
      opt2: "Cuff your hands together for 20minutes.",
      pass1: "Cuff both your legs and hands for 30minutes.",
      pass2:
        "Tie yourself in a hogtie/frog-tie/lotus-tie/mummy-tie for 15minutes.",
      tags: "slave"
    },
    314: {
      id: "314",
      imgUrl: "#",
      type: "class",
      name: "Heavy Bondage 314",
      name2: "奴隶",
      prerequisites: "214",
      days: "2 4",
      description: "You will learn to endure pain and punishment. ",
      tier: "advanced",
      opt1:
        "Attach clothespins to your nipples for 5minutes and spank your ass 20times.",
      opt2:
        "Tie yourself in a hogtie/frog-tie/lotus-tie/mummy-tie tie for 15minutes.",
      pass1:
        "Attach clothespins to your nipples for 10minutes and spank your ass 40times.",
      pass2:
        "Tie yourself in a hogtie/frog-tie/lotus-tie/mummy-tie for 20minutes while blindfolded and gagged.",
      tags: "slave"
    },
    414: {
      id: "414",
      imgUrl: "#",
      type: "class",
      name: "Masochism 414",
      name2: "奴隶",
      prerequisites: "314",
      days: "2 3",
      description: "",
      tier: "master",
      opt1:
        "Attach clothespins to your nipples for 10minutes and spank your ass 30times. Then slap your balls/pussy 5times.",
      opt2:
        "Tie yourself in a hogtie/frog-tie/lotus-tie/mummy-tie for 20minutes while blindfolded and gagged.",
      pass1:
        "Attach clothespins to your nipples and tie yourself in a hogtie/frog-tie/lotus-tie/mummy-tie for 15minutes while blindfolded and gagged.",
      pass2:
        "Gag yourself and attach clothespins to your nipples. Spank yourself hard at least 30times on each butt cheek with an object. Then slap your balls/pussy 10times.",
      tags: "slave"
    },
    215: {
      id: "215",
      imgUrl: "#",
      type: "class",
      name: "Sex Training 215",
      name2: "口穴 菊穴",
      prerequisites: "102 105",
      days: "2 4",
      description: "Time to put your oral and anal skills to the test.",
      tier: "intermediate",
      opt1:
        "Spitroast yourself for 8minutes with two dildos (sizes S and M)",
      opt2:
        "Fuck yourself in the ass with a dildo (size M) for 5minutes and then give it a 5minutes blowjob.",
      pass1:
        "Spitroast yourself for 15minutes with two dildos (sizes S and M)",
      pass2:
        "Fuck yourself in the ass with a dildo (size M) for 10minutes and then give it a 5minutes blowjob.",
      tags: "penetration"
    },
    415: {
      id: "415",
      imgUrl: "#",
      type: "class",
      name: "Double Penetration 415",
      name2: "口穴 菊穴",
      prerequisites: "215 302 305",
      days: "2 4",
      description: "You will be trained to take multiple toys at once.",
      tier: "master",
      opt1:
        "Spitroast yourself for 15minutes with two dildos (sizes M and L)",
      opt2:
        "Fuck yourself in the ass with a dildo (size L) for 10minutes. Leave it in and then give another one (size M) a 5minutes blowjob with at least 10 deepthroats.",
      pass1:
        "Spitroast yourself for 25minutes with two dildos (sizes M and L)",
      pass2:
        "Fuck yourself in the ass with a dildo (size L) for 15minutes. Leave it in and then give another one (size M) a 10minutes blowjob with at least 30 deepthroats.",
      tags: "anal oral"
    },
    216: {
      id: "216",
      imgUrl: "#",
      type: "class",
      name: "Exposure 216",
      name2: "暴露狂",
      prerequisites: "",
      days: "1 4",
      description: "You will overcome your shyness.",
      tier: "intermediate",
      opt1: "Perform one of your daily tasks with the window open.",
      opt2:
        "Tell the public how your daily tasks go. This can be in chat or online (4chan/reddit/)",
      pass1:
        "Go outside while wearing an item from your other classes (chastity, buttplug, dildo etc). The item can be hidden under your clothes.",
      pass2:
        "Take photos while performing your daily tasks and share them to the public (in chat or online)",
      tags: "exhibitionism"
    },
    316: {
      id: "316",
      imgUrl: "#",
      type: "class",
      name: "Publicity 316",
      name2: "暴露狂",
      prerequisites: "216",
      days: "1 4",
      description: "Time to come out of the closet.",
      tier: "advanced",
      opt1:
        "Go outside while wearing an item from your other classes (chastity, buttplug, dildo etc). The item can be hidden under your clothes.",
      opt2:
        "Take photos while performing your daily tasks and share them to the public (in chat or online).",
      pass1:
        "Go outside and take a photo that shows you wearing an item from another class. The photo can be taken at a semi-public place (bathroom, changing room etc). Share the photo in chat or online.",
      pass2:
        "Record a 5 minute video of yourself while performing another task from the university and share it publicly.",
      tags: "exhibitionism"
    },
    416: {
      id: "416",
      imgUrl: "#",
      type: "class",
      name: "Exhibitionism 416",
      name2: "暴露狂",
      prerequisites: "316",
      days: "1 4",
      description: "You completely overcome your instinct towards shame.",
      tier: "master",
      opt1:
        "Go outside and take a photo that shows you wearing an item from another class. The photo must be taken at a semi-public place (bathroom, changing room etc). Share the photo in chat or online.",
      opt2:
        "Record a 5 minute video of yourself performing another university task and share it in chat or online.",
      pass1:
        "Go outside and take a photo that shows you wearing an item from another class. The photo must be taken at a public place (park, street etc.). Share the photo in chat or online.",
      pass2:
        "Live-stream your university tasks and share it publicly. (Minimum 10 minutes)",
      tags: "public"
    },
    217: {
      id: "217",
      imgUrl: "#",
      type: "class",
      name: "Pet play 117",
      name2: "宠物化",
      prerequisites: "",
      days: "1 4 5",
      description: "You will start exploring the pet play lifestyle.",
      tier: "beginner",
      opt1: "Wear animal ears and a collar for 60minutes today.",
      opt2: "Wear animal ears, a collar and a tail for 30minutes today.",
      pass1: "Wear animal ears, a collar and a tail for 60minutes today.",
      pass2:
        "Wear animal ears, a collar and a tail for 30minutes today. Walk on all fours.",
      tags: "pet"
    },
    317: {
      id: "317",
      imgUrl: "#",
      type: "class",
      name: "Pet Training 317",
      name2: "宠物化",
      prerequisites: "217",
      days: "2 4",
      description: "You will learn to behave like a pet.",
      tier: "advanced",
      opt1: "Wear animal ears, a collar and a tail for 60minutes today.",
      opt2:
        "Wear animal ears, a collar and a tail for 30minutes today. Walk on all fours.",
      pass1:
        "Wear animal ears, a collar and a tail for 120minutes today.",
      pass2:
        "Wear animal ears, a collar and a tail for 60minutes today. Walk on all fours. Eat and drink from a bowl on the floor.",
      tags: "pet"
    },
    417: {
      id: "417",
      imgUrl: "#",
      type: "class",
      name: "Animal Training 417",
      name2: "宠物化",
      prerequisites: "317",
      days: "2 4",
      description: "You will become a human pet.",
      tier: "master",
      opt1:
        "Wear animal ears, a collar and a tail for 60minutes today. Walk on all fours. Eat and drink from a bowl on the floor.",
      opt2:
        "Wear animal ears, a collar and a buttplug tail for 30minutes today. Tie yourself in a bitchsuit position.",
      pass1:
        "Wear animal ears, a collar and a tail for 120minutes today. Walk on all fours. Eat and drink from a bowl on the floor.",
      pass2:
        "Wear animal ears, a collar and a buttplug tail for 60minutes today. Tie yourself in a bitchsuit position.",
      tags: "pet"
    },
    220: {
      id: "220",
      imgUrl: "#",
      type: "class",
      name: "Obedience 220",
      name2: "奴隶",
      prerequisites: "",
      days: "2 3",
      description: "You will learn to follow command.",
      tier: "intermediate",
      opt1: "Roll a random Light Punishment 1times.",
      opt2: "Request a slave task in chat or online 1times.",
      pass1: "Roll a random Light Punishment 2times.",
      pass2: "Request a slave task in chat or online 2times.",
      tags: "slave"
    },
    320: {
      id: "320",
      imgUrl: "#",
      type: "class",
      name: "Submission 320",
      name2: "奴隶",
      prerequisites: "220",
      days: "2 3",
      description: "You will learn to obey your master's commands.",
      tier: "advanced",
      opt1: "Roll a random Light Punishment 2times.",
      opt2: "Request a slave task in chat or online 2times.",
      pass1: "Roll a random Heavy Punishment 1times.",
      pass2: "Request a hard slave task in chat or online 3times.",
      tags: "slave"
    },
    420: {
      id: "420",
      imgUrl: "#",
      type: "class",
      name: "Slavery 420",
      name2: "奴隶",
      prerequisites: "320",
      days: "2 3 4",
      description: "You will learn to obey your master's commands.",
      tier: "master",
      opt1:
        "Roll a random Heavy Punishment and a random Light Punishment 1times.",
      opt2: "Request a hard slave task in chat or online 3times.",
      pass1:
        "Roll a random Heavy Punishment and a random Light Punishment 2times.",
      pass2: "Request a hard slave task in chat or online 4times.",
      tags: "slave"
    }
  };
}